const BASE_URL = 'http://127.0.0.1:8000';

import BaseController from './BaseController.js';

import dataService from '../services/DataService.js';

export default class NewAdvertController extends BaseController {

    constructor(element) {
        super(element);
        this.checkIfUserIsLogged();
        this.attachEventListener();
    };

    async checkIfUserIsLogged() {
        const userIsLog = await dataService.isUserLogged();
        if (!userIsLog) {
            window.location.href = '/login.html?next=/new-tweet.html';
        } else {
            this.publish(this.events.FINISH_LOADING);
        }
    };

    checkInputErrors() {
        let isValid = true;
        let isCheck = false;
        let isCheckBox = false;

        this.element.querySelectorAll('input').forEach(input => {
            const button = this.element.querySelector('button');
            if (input.type == 'radio') {
                const group = this.element.elements.sale;
                for( const radio of group ){
                    if(radio.checked){
                        isCheck = true;
                    }
                } 
            }
            if (input.type == 'checkbox') {
                const group = this.element.elements.tags;
                for( const check of group ){
                    if(check.checked){
                        isCheckBox = true;
                    }
                } 
            }
            if (input.type != 'file') {

                if (input.value) {
                    //Se entra si hay algo
                    if (input.name == 'price') {
                        if (isNaN(input.value)) {
                            input.classList.add('is-bad');
                            isValid = false;
                        } else {
                            input.classList.remove('is-bad');
                        }
                    }
                } else {
                    isValid = false;
                };

                if (isValid && isCheck && isCheckBox) {
                    button.removeAttribute('disabled');
                } else {
                    button.setAttribute('disabled', true);
                }
            };
        });
    };

    getCkeckedTags(){
        const tagArray = [];
        const groupTags = this.element.elements.tags;
        for( const tag of groupTags){
            if(tag.checked){
                tagArray.push(tag.value);
            }
        }
        return tagArray;
    }

    attachEventListener() {
        this.element.querySelectorAll('input').forEach(input => {
            const button = this.element.querySelector('button');
            input.addEventListener('keyup', event => {
                this.checkInputErrors();
            });
        });

        this.element.querySelectorAll('input[type="radio"]').forEach(input => {
            const button = this.element.querySelector('button');
            input.addEventListener('click', event => {
                this.checkInputErrors();
            });
        });
        
        this.element.querySelectorAll('input[type="checkbox"]').forEach(input =>{
            const button = this.element.querySelector('button');
            input.addEventListener('click', event=>{
                this.checkInputErrors();
            });
        });

        this.element.addEventListener('submit', async event => {
            event.preventDefault();
            const advert = {
                name: this.element.elements.nameproduct.value,
                price: this.element.elements.price.value,
                sale: this.element.elements.sale.value,
                tags: this.getCkeckedTags(),
                photo: null
            }
            if (this.element.elements.file.files.length > 0) {
                advert.photo = this.element.elements.file.files[0];
            } else {
                advert.photo = `${BASE_URL}/default.jpg`;
            }
            this.publish(this.events.START_LOADING);
            try {
                await dataService.saveAdvert(advert);
                window.location.href = '/?mensaje=advertOK';
            } catch (error) {
                this.publish(this.events.ERROR, error);
            } finally {
                this.publish(this.events.FINISH_LOADING);
            }
        });
    };



}