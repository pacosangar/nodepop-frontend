const BASE_URL = 'http://127.0.0.1:8000';

import BaseController from './BaseController.js';

import dataService from '../services/DataService.js';

import { checkTags } from '../views.js';

export default class TagsController extends BaseController{

    constructor(element){
        super(element);
    }

     //Mostramos por pantalla los tags
     render(tags){
        this.element.innerHTML = ''; 
        for (const tag of tags){
            const div = document.createElement('div');
            div.innerHTML = checkTags(tag);
            this.element.appendChild(div);
        }
    }

    //Cargamos los tags desde el servidor y los mostramos
    async loadTags(){
        
        this.publish(this.events.START_LOADING, {});
        try{
            const tags = await dataService.getTags(); //Devuelve una promesa.
            this.render(tags); 
        }catch(error){
            this.publish(this.events.ERROR, error);
        }finally{
            //Esto se ejecuta siempre, vaya bien o vaya mal.
            this.publish(this.events.FINISH_LOADING, {});
        }
    }

}