import BaseController from './BaseController.js';

import dataService from '../services/DataService.js';

export default class LoginController extends BaseController {
    
    constructor(element) {
        super(element);
        this.attachEventListener();
      }
    
    //Debera encargarse comprobar que hemos metido todos los datos y que son correctos.
    checkInputData() {
        this.element.querySelectorAll('input').forEach(input => {
            const button = this.element.querySelector('button');
            if (input.validity.valid) {
                input.classList.add('is-focus');
                input.classList.remove('is-bad');

            } else {
                input.classList.remove('is-focus');
                input.classList.add('is-bad');
            }
            //Cumpruebo si todos los campos del formulario estan rellenos con un metodo propio de los formularios
            if (this.element.checkValidity()) {
                button.removeAttribute('disabled'); //Habilitamos el bóton
                //button.setAttribute('disabled', false); //hacen lo mismo
            } else {
                button.setAttribute('disabled', true); //Deshabilitamos el bóton
            }

        });

    } //Fin checkImputData

    attachEventListener() {
        this.element.addEventListener("submit", async (event) => {
            event.preventDefault(); //Deshabilitamos el comportamiento por defecto del submit que es enviar el formulario
            const user = {
                username: this.element.elements.email.value,
                password: this.element.elements.password.value,
            }
            this.publish(this.events.START_LOADING);
            try {
                const data = await dataService.loginUser(user);
                dataService.saveToken(data.accessToken);
                let next = '/';
                const queryParams = window.location.search.replace('?', '');  // ?next=otrapagina -> next=otrapagina
                const queryParamsParts = queryParams.split('=');
                if (queryParamsParts.length >= 2 && queryParamsParts[0] === 'next') {
                    next = queryParamsParts[1];
                }
                window.location.href = next;
            } catch (error) {
                this.publish(this.events.ERROR , error);
            } finally {
                this.publish(this.events.FINISH_LOADING);
            }
        });

        //Tecleado en los todos inputs
        this.element.querySelectorAll('input').forEach(input => {
            const button = this.element.querySelector('button');
            input.addEventListener('keyup', event => {
                //Comprobamos si se han rellenado el input
                this.checkInputData();
            });
        });
    };




}