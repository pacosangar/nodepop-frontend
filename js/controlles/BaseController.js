'use strict';

import pubSub from '../services/PubSub.js';

export default class BaseController {

    constructor(element) {
        this.element = element;
        //Para que todos nuestros controladores tengan el pubSub lo ponemos en la clase madres.
        this.pubSub = pubSub;
        this.events = {
            START_LOADING: 'startLoading',
            FINISH_LOADING: 'finishLoading',
            ERROR: 'error',
        };
    }

    subscribe(eventName, eventHandler) {
        this.pubSub.subscribe(eventName, eventHandler);
    }

    publish(eventName, eventData) {
        this.pubSub.publish(eventName, eventData);
    }

}
