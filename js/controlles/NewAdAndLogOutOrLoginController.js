import BaseController from './BaseController.js';

import dataService from '../services/DataService.js';

/**
 * Mostramos los iconos de nuevo anuncio y Sign Out, en caso de que estemos logados,
 * sino estamos logados mostramos icono de Sign In.
 */

export default class NewAdAndLogOutOrLoginController extends BaseController{

    constructor(element){
        super(element);
        this.checkIfUserIsLogged();
    }

    async checkIfUserIsLogged(){
        const userIsLoged = await dataService.isUserLogged();
        if (userIsLoged){
            //Muestro iconos de new Ad y Sign Out
            const logoutIcon = this.element.querySelector('.logout');
            logoutIcon.classList.remove('hidden');
            const newAdIcon = this.element.querySelector('.newAd');
            newAdIcon.classList.remove('hidden');
        }else{
            //Muestro icono de Sign In
            const loginIcon = this.element.querySelector('.login');
            loginIcon.classList.remove('hidden');
        }
    }






}