
import BaseController from './BaseController.js';

import dataService from '../services/DataService.js';

export default class LogoutControler extends BaseController{
    constructor(element){
        super(element);
        this.element.addEventListener('click', event =>{
            dataService.logoutUser();
            //this.publish(this.events.LOGOUT)
        });
    }
}

