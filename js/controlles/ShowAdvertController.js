import BaseController from './BaseController.js';
import dataService from '../services/DataService.js';
import { adView } from '../views.js';

/**
 * Se encargara de coger el modelo pasarlo a la vista y que esta lo muestre al usuario
 */

export default class ShowAdvertController extends BaseController{
    
    constructor (element){
        super(element);
    }

    //Mostramos por pantalla los anuncios
    render(advertisements){
        this.element.innerHTML = ''; //element sera -> grid-content-ads
        for (const advert of advertisements){
            const article = document.createElement('article');
            article.innerHTML = adView(advert);

            const deleteIcon = article.querySelector('.deleteIcon');
            if(deleteIcon){ //Si tengo icono de papelera pues delete
                deleteIcon.addEventListener('click', async event =>{
                    //Llamo al método que borra el anuncio, pasando el anuncio como parametro
                    if(confirm('¿Seguro que quieres BORRAR el anuncio')){
                        await dataService.deleteAdvert(advert);
                        article.remove(); // Borra el anuncio que hemos eliminado
                        //Si queremos recagar los anuncios
                        //await this.loadAds();
                        window.location.href ='index.html';
                    } 
                }); 
            }


            this.element.appendChild(article);
        }
    }

    //Cargamos los anuncios desde el servidor y los mostramos
    async loadAds(){
        const url = window.location.search;
        this.publish(this.events.START_LOADING, {});
        try{
            const advertisements = await dataService.getAdDetail(url); //Devuelve una promesa.
            this.render(advertisements); 
        }catch(error){
            this.publish(this.events.ERROR, error);
        }finally{
            //Esto se ejecuta siempre, vaya bien o vaya mal.
            this.publish(this.events.FINISH_LOADING, {});
        }
    }
}