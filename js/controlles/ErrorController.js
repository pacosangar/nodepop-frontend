
import BaseController from './BaseController.js';

import { errorView } from '../views.js';

export default class ErrorController extends BaseController{

    constructor(element){
        super(element);
        //Cuando se produzca el evento de error, muestra el error
        this.subscribe(this.events.ERROR , (error) => {
            this.showError(error);
        });
    };

    showError(error){
        //Cargo la vista del error
        this.element.innerHTML = errorView(error);
        this.element.classList.remove('hidden');
        //Capturamos el evento para cerrar el error
        this.element.addEventListener('click', (event) =>{
            if (event.target == this.element || event.target.classList.contains('delete')) {
                this.hideError();
            }
        });

        //Los this no cogen los eventos de teclado.
        document.body.addEventListener('keydown', (event) =>{
            if(event.key === "Escape"){
                this.hideError();
            }
        });
    }

    hideError(){
        this.element.classList.add('hidden');
    }
}