
import LoaderController from './controlles/LoaderController.js';

import ErrorController from './controlles/ErrorController.js';

import LoginController from './controlles/LoginController.js';

//Grandizamos que todo este listo
window.addEventListener('DOMContentLoaded', (event) => {
     
    //Para controlar el loader, ocultarlo y mostrarlo.
    const loaderElement = document.querySelector('.lds-spinner');
    const loaderController = new LoaderController(loaderElement);

     //Muestro el error en caso de que suceda
     const errorElement = document.querySelector('.global-errors');
     const errorController = new ErrorController(errorElement);
    
    //Controlamos el logino del usuario
    const loginElement = document.querySelector('.login-form');
    new LoginController(loginElement);
});
