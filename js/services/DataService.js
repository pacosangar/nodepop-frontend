const BASE_URL = 'http://127.0.0.1:8000';

const TOKEN_KEY = 'token';
/**
 * Capa de abtracción de los datos
 */
export default {

    /*Recuperamos los anuncios, nos da igual donde se encuentren aquí los recuperamos
     * para usarlos.
    */

    getAds: async function (query = null) {

        const currentUser = await this.getUser(); //Recuperamos el usuario actual

        let url = `${BASE_URL}/api/advert?_sort=id&_order=desc`;

        /*
        if (query) { //Para shearch para modificar la query y debuelva lo que coincida...
            url += `&q=${query}`
        }*/
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            return data.map(advert => {
                return {
                    id: advert.id,
                    name: advert.name,
                    sale: advert.sale,
                    price: advert.price,
                    photo: advert.photo || null,
                    tags: advert.tags,
                    //operador ternario
                    //El anuncio es borrable si usuario esta logado y si el anuncio le pertenece.
                    canBeDeleted: currentUser ? currentUser.userId === advert.userId : false,
                }
            });
        } else {
            throw new Error(`HTTP Error: ${response.status}`);
        }
    },

    getAdDetail: async function (id) {

        const currentUser = await this.getUser(); //Recuperamos el usuario actual
        let url = `${BASE_URL}/api/advert/${id}`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            return data.map(advert => {
                return {
                    id: advert.id,
                    name: advert.name,
                    sale: advert.sale,
                    price: advert.price,
                    photo: advert.photo || null,
                    tags: advert.tags,
                    //operador ternario
                    //El anuncio es borrable si usuario esta logado y si el anuncio le pertenece
                    canBeDeleted: currentUser ? currentUser.userId === advert.userId : false,
                }
            });
        } else {
            throw new Error(`HTTP Error: ${response.status}`);
        }
    },


    post: async function (url, postData, json = true) {
        return await this.request('POST', url, postData, json);
    },

    delete: async function (url) {
        return await this.request('DELETE', url, {});
    },

    put: async function (url, putData, json = true) {
        return await this.request('PUT', url, putData, json);
    },

    request: async function (method, url, postData, json = true) {

        const config = {
            method: method,
            headers: {},
            body: null
        };
        if (json) {
            config.headers['Content-Type'] = 'application/json';
            config.body = JSON.stringify(postData);  // convierte el objeto de usuarios en un JSON
        } else {
            config.body = postData;
        }
        const token = await this.getToken();
        if (token) {
            config.headers['Authorization'] = `Bearer ${token}`;
        }
        const response = await fetch(url, config);
        const data = await response.json();  // respuesta del servidor sea OK o sea ERROR.
        if (response.ok) {
            return data;
        } else {
            // TODO: mejorar gestión de errores
            // TODO: si la respuesta es un 401 no autorizado, debemos borrar el token (si es que lo tenemos);
            throw new Error(data.message || JSON.stringify(data));
        }

    },

    registerUser: async function (user) {
        const url = `${BASE_URL}/auth/register`;
        return await this.post(url, user);
    },

    loginUser: async function (user) {
        const url = `${BASE_URL}/auth/login`;
        return await this.post(url, user);
    },

    logoutUser: async function () {
        return localStorage.removeItem(TOKEN_KEY);
    },

    saveToken: async function (token) {
        localStorage.setItem(TOKEN_KEY, token);
    },

    getToken: async function () {
        return localStorage.getItem(TOKEN_KEY);
    },

    isUserLogged: async function () {
        const token = await this.getToken();
        return token !== null;  // esto devuelve true o false
    },

    saveAdvert: async function (advert) {
        const url = `${BASE_URL}/api/advert`;
        if (advert.photo != `${BASE_URL}/default.jpg`) {
            const imageURL = await this.uploadImage(advert.photo);
            advert.photo = imageURL;
        }
        return await this.post(url, advert);
    },

    uploadImage: async function (image) {
        const form = new FormData();
        form.append('file', image);
        const url = `${BASE_URL}/upload`;
        const response = await this.post(url, form, false);
        return response.path || null;
    },

    getUser: async function () {
        try {
            const token = await this.getToken();
            const tokenParts = token.split('.');
            if (tokenParts.length !== 3) {
                return null;
            }
            const payload = tokenParts[1]; // cogemos el payload, codificado en base64
            const jsonStr = atob(payload); // descodificamos el base64
            const { userId, username } = JSON.parse(jsonStr); // parseamos el JSON del token descodificado
            return { userId, username };
        } catch (error) {
            return null;
        }
    },

    deleteAdvert: async function (advert) {
        const url = `${BASE_URL}/api/advert/${advert.id}`;
        return await this.delete(url);
    },

    getTags: async function () {
        let url = `${BASE_URL}/api/tags`;
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            return data;
        } else {
            throw new Error(`HTTP Error: ${response.status}`);
        }
    },
};