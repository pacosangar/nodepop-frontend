import ShowAdvertController from './controlles/ShowAdvertController.js';

import LoaderController from './controlles/LoaderController.js';

import ErrorController from './controlles/ErrorController.js';

import LogoutController from './controlles/LogoutControler.js';

import NewAdAndLogOutOrLoginController from './controlles/NewAdAndLogOutOrLoginController.js';


//Grandizamos que todo este listo
window.addEventListener('DOMContentLoaded', (event) => {
     
    //Para controlar el loader, ocultarlo y mostrarlo.
    const loaderElement = document.querySelector('.lds-spinner');
    const loaderController = new LoaderController(loaderElement);

    //Muestro el anuncios
    const element = document.querySelector('.grid-content-ads');
    const controller = new ShowAdvertController(element);
    controller.loadAds();

    //Muestro el error en caso de que suceda
    const errorElement = document.querySelector('.global-errors');
    const errorController = new ErrorController(errorElement);

    //Control de iconos con funcionalidad
    const controlIcons = document.querySelector('.nav-bar');
    new NewAdAndLogOutOrLoginController(controlIcons);


    //Controlamos si se pulsa logout
    const logoutElement = document.querySelector('.logout');
    new LogoutController(logoutElement);
    
});

