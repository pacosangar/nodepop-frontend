import {escapeHTML} from './utils.js';


export const adView = (advert) => {

    let deleteIconHTML = '';

    if (advert.canBeDeleted) {
        deleteIconHTML = `<div class="icon">
                                <i class="fas fa-trash-alt deleteIcon"></i>
                          </div>`;
    }

    let saleHTML = '';

    if (advert.sale) {
        saleHTML = `<i class="fas fa-credit-card"></i>`;
    } else {
        saleHTML = `<i class="fas fa-binoculars"></i>`;
    }

    return `<article class="grid-item">
                <div class="adbody">
                <img src="${escapeHTML(advert.photo)}" alt="${escapeHTML(advert.name)}">
                <div class="info">
                    <h3>${escapeHTML(advert.price)} €</h3>
                    <p>${escapeHTML(advert.name)}</p>
                    <p>${advert.tags}</p>
                    <h4>
                        ${saleHTML}
                    </h4>
                </div>
                </div>
                ${deleteIconHTML}
                <div class="nofloat"><div>
               
            </article>`
};

export const errorView = (error) => {
    return `<article class="message is-danger">
                <div class="message-header">
                    <p>Error</p>
                    <button class="delete" aria-label="delete"></button>
                </div>
                <div class="message-body">
                    ${error}
                </div>
            </article>`
};

export const tagsView = (tags) => {
    return ` <div class="nav-tag-items"><i class="fas fa-book-dead"></i></div>
             <div class="nav-tag-items"><i class="fas fa-car"></i></div>
             <div class="nav-tag-items"><i class="fas fa-baby"></i></div>
             <div class="nav-tag-items"><i class="fab fa-pied-piper-hat"></i></div>
             <div class="nav-tag-items"><i class="fas fa-running"></i></div>`
};

export const checkTags = (tags) => {
    return ` <div><input type="checkbox" id="${tags.tagName}" name="tags" value="${tags.tagName}"> <label for="${tags.tagName}">${tags.tagName}</label></div>`
}