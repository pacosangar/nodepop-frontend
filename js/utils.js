

export function escapeHTML(unsafe) {
    unsafe = unsafe.toString();
    return unsafe.replace(
      /[\u0000-\u002F]|[\u003A-\u0040]|[\u005B-\u00FF]/g,
      c => '&#' + ('000' + c.charCodeAt(0)).substr(-4, 4) + ';'
    )
  }
