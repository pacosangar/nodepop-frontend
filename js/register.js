
import LoaderController from './controlles/LoaderController.js';
import ErrorController from './controlles/ErrorController.js';
import RegisterController from './controlles/RegisterController.js';

//Grandizamos que todo este listo
window.addEventListener('DOMContentLoaded', (event) => {
     
    //Para controlar el loader, ocultarlo y mostrarlo.
    const loaderElement = document.querySelector('.lds-spinner');
    const loaderController = new LoaderController(loaderElement);

     //Muestro el error en caso de que suceda
    const errorElement = document.querySelector('.global-errors');
    new ErrorController(errorElement);
    
    //Controlamos el registro del usuario
    const registerElement = document.querySelector('.register-form');
    const registerController = new RegisterController(registerElement);
    
});

