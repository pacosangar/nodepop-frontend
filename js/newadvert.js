import LoaderController from './controlles/LoaderController.js';

import ErrorController from './controlles/ErrorController.js';

import LogoutController from './controlles/LogoutControler.js';
import NewAdAndLogOutOrLoginController from './controlles/NewAdAndLogOutOrLoginController.js';
import NewAdvertController from './controlles/NewAdvertController.js';
import TagsController from './controlles/TagsController.js';

//Grandizamos que todo este listo
window.addEventListener('DOMContentLoaded', (event) => {
     
    //Para controlar el loader, ocultarlo y mostrarlo.
    const loaderElement = document.querySelector('.lds-spinner');
    const loaderController = new LoaderController(loaderElement);

    //Muestro el error en caso de que suceda
    const errorElement = document.querySelector('.global-errors');
    const errorController = new ErrorController(errorElement);

    //Control de iconos con funcionalidad
    const controlIcons = document.querySelector('.nav-bar');
    new NewAdAndLogOutOrLoginController(controlIcons);

    //Controlamos si se pulsa logout
    const logoutElement = document.querySelector('.logout');
    new LogoutController(logoutElement);
    
    //Control nuevo anuncio
    const newAdvert = document.querySelector('.form-new-ad');
    new NewAdvertController(newAdvert);

    //Los tag dinamicos
    const checkBoxTags = document.querySelector('.check-tags');
    const tagsController = new TagsController(checkBoxTags);
    tagsController.loadTags();
});
